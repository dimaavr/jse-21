package ru.tsc.avramenko.tm.exception.empty;

import ru.tsc.avramenko.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {

    public EmptyRoleException() {
        super("Error! Role is empty.");
    }

}
