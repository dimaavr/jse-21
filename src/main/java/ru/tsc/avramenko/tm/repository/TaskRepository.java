package ru.tsc.avramenko.tm.repository;

import ru.tsc.avramenko.tm.api.repository.ITaskRepository;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.model.Task;

import java.util.*;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAll(final String userId, final Comparator<Task> comparator) {
        return list.stream()
                .filter(t -> t.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findById(userId, id) != null;
    }

    @Override
    public Task findByName(final String userId, final String name) {
        return findAll(userId).stream()
                .filter(t -> t.getName().equals(name))
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public Task findByIndex(final String userId, int index) {
        final List<Task> tasks = findAll(userId);
        return tasks.get(index);
    }

    @Override
    public Task removeByName(final String userId, final String name) {
        final Optional<Task> task =  Optional.ofNullable(findByName(userId, name));
        task.ifPresent(this::remove);
        return task.orElseThrow(ProcessException::new);
    }

    @Override
    public Task removeByIndex(final String userId, final int index) {
        final Optional<Task> task =  Optional.ofNullable(findByIndex(userId, index));
        task.ifPresent(this::remove);
        return task.orElseThrow(ProcessException::new);
    }

    @Override
    public Task startById(final String userId, final String id) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByIndex(final String userId, final int index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task finishById(final String userId, final String id) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Override
    public Task finishByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Override
    public Task finishByIndex(final String userId, final int index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Override
    public Task changeStatusById(final String userId, final String id, final Status status) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(final String userId, final String name, final Status status) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final String userId, final int index, final Status status) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String userId, String id) {
        return findAll(userId).stream()
                .filter(t -> t.getUserId().equals(userId))
                .filter(t -> t.getProjectId().equals(id))
                .collect(Collectors.toList());
    }

    @Override
    public Task bindTaskToProjectById(final String userId, String projectId, String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        task.setUserId(userId);
        return task;
    }

    @Override
    public Task unbindTaskById(final String userId, String id) {
        final Task task = findById(userId, id);
        task.setUserId(null);
        task.setProjectId(null);
        return task;
    }

    @Override
    public void unbindAllTaskByProjectId(final String userId, String id) {
        findAll(userId).stream()
                .filter(t -> t.getProjectId().equals(id))
                .filter(t -> t.getUserId().equals(userId))
                .forEach(t -> t.setProjectId(null));

    }

}